var express = require('express');
var app = express();

const addMinutes = function(date, minutes) {
    const d = new Date(date);
    d.setMinutes( date.getMinutes() + minutes );
    return d;
}
const randBetween = (from, to) => Math.floor(Math.random() * to) + from;

const chance = (perc) => randBetween(0, 100) < perc * 100;

const getRandomStatus = () => {
    if (chance(.8)) {
        return 200;
    } else if (chance(.5)) {
        return 404;
    } else {
        return 500;
    }
}

const devices = [
    {
        "deviceId": 1,
        "ip": "google.com",
        "name": "John"
    },
    {
        "deviceId": 2,
        "ip": "bing.com",
        "name": "Bingy"
    },
    {
        "deviceId": 3,
        "ip": "moep.com",
        "name": "Blub"
    },
 ];
 
const endpoints = [
    {
        "endpointId": 1,
        "url": "google.com",
        "name": "John",
        "method": "GET"
    },
    {
        "endpointId": 2,
        "url": "bing.com",
        "name": "Bingy",
        "method": "GET"
    },
    {
        "endpointId": 3,
        "url": "moep.com",
        "name": "Blub",
        "method": "POST"
    },
 ];

app.get('/api/device/', function (req, res) {
    res.send(JSON.stringify(devices));
});

app.get('/api/device/:id', function (req, res) {
    const device = devices.find(d => d.deviceId == req.params.id);
    let date = new Date();
    date.setHours(date.getHours() - 1);
    const pings = '.'.repeat(randBetween(10,60)).split('').map((_, i) => {
        date = addMinutes(date, randBetween(1, 3));

        return {
            status: randBetween(0, 100) < 70 ? 'online' : 'offline',
            date
        };
    });

    res.send(JSON.stringify({
        "deviceId": device.deviceId,
        "ip": device.ip,
        "name": device.name,
        "status": pings
     }));
});


app.get('/api/endpoint/', function (req, res) {
    res.send(JSON.stringify(endpoints));
});

app.get('/api/endpoint/:id', function (req, res) {
    const ep = endpoints.find(e => e.endpointId == req.params.id);
    let date = new Date();
    date.setHours(date.getHours() - 1);
    const pings = '.'.repeat(randBetween(10,60)).split('').map((_, i) => {
        date = addMinutes(date, randBetween(1, 3));

        return {
            statusMessage: 'SMes',
            statusCode: getRandomStatus(),
            date
        };
    });

    res.send(JSON.stringify({
        "endpointId": ep.endpointId,
        "url": ep.url,
        "name": ep.name,
        "method": ep.method,
        "status": pings
     }));
});

app.listen(8080, function () {
  console.log('Example app listening on port 8080!');
});